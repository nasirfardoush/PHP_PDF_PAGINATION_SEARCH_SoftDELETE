<?php 
	include_once("../../vendor/autoload.php");
	include_once("../../vendor/mpdf/mpdf/mpdf.php");
	use App\employee\Employee;
	$empObj = new Employee;
	$empInfo = $empObj->index();

	 $emplist='';
	 $Slno = 1;
	 foreach($empInfo as $info){
	 	$emplist.="<tr>";
	 	$emplist.="<td>".$Slno++."</td>";
	 	$emplist.="<td>".$info['emp_title']."</td>";
	 	$emplist.="<td>".$info['emp_designation']."</td>";
	 	$emplist.="</tr>";
	 }

$html=<<<EOD
<!DOCTYPE html>
<html>
<head>
	<title>List of employee</title>
	<style>
			table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
	</style>
</head>
<body>
	<section class="mainContent">
		<table>
			<tr>
				<th colspan="3">Empoloyee List</th>
			</tr>
			<tr>
				<th>Sl no:</th>
				<th>Name</th>
				<th>Dedignation</th>
			</tr>
			$emplist
		</table>
		
	</section>
</body>
</html>
EOD;

$objpdf = new mPDF();
$objpdf->WriteHTML($html);
$objpdf->Output();
exit;
?>

