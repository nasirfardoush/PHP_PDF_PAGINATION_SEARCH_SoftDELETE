<?php 
	include_once("../../vendor/autoload.php");
	use App\employee\Employee;
	$empObj = new Employee;
	$empDetails = $empObj->setData($_GET)->show();
?>

<!DOCTYPE html>
<html>
<head>
	<title>List of employee</title>
</head>
<body>
	<section class="mainContent">
		<table align="center" border="1px" cellspacing="0" style="padding: 20px; background:#fcfcfc;">
<tr>
				<th colspan="5">Empoloyee Details</th>
			</tr>					
			<tr>
				<th colspan="1"><a href="index.php">Employee List</a></th>
				<th colspan="1"><a href="create.php">Add Employee</a></th>
				<th colspan="3"><a href="#">Dowanload as pdf</a></th>
			</tr>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Dedignation</th>
				<th>Salary</th>
				<th>Action</th>
			</tr>
			<tr>
				<td><?php echo $empDetails['emp_id']; ?></td>
				<td><?php echo $empDetails['emp_title']; ?></td>
				<td><?php echo $empDetails['emp_designation']; ?></td>
				<td><?php echo $empDetails['emp_salary']; ?></td>
				<td>
					<a href="edit.php?id=<?php echo $empDetails['emp_id']; ?>">Edit</a>||
					<a href="softDelete.php?id=<?php echo $empDetails['emp_id']; ?>">Delete</a>
				</td>
			</tr>	
		</table>
		
	</section>
</body>
</html>