<?php 
	include_once("../../vendor/autoload.php");
	use App\employee\Employee;
	$empObj = new Employee;
	$numbOfItemPerPage = 10;
	if (isset($_GET['pageNo'])) {
		$numberOfPage = $_GET['pageNo']-1;
	}else{$numberOfPage = 0;}
	$offsetdata = $numbOfItemPerPage * $numberOfPage;

	$empInfo = $empObj->index($numbOfItemPerPage, $offsetdata);
	$totalRows = array_pop($empInfo);
	$numberOfPage = ceil($totalRows / $numbOfItemPerPage);
	echo $numberOfPage;
?>

<!DOCTYPE html>
<html>
<head>
	<title>List of employee</title>
</head>
<body>
	<section class="mainContent">
		<table align="center" border="1px" cellspacing="0" style="padding: 20px; background:#fcfcfc;">
			<tr>
				<th colspan="4">Empoloyee List</th>
			</tr>
			<tr>
				<?php if(isset($_SESSION['msg'])){ ?>
					<th colspan="4">
						<?php	
							echo "<h3>".$_SESSION['msg']."</h3>";
							unset($_SESSION['msg']); ?>
					</th>	 
				<?php }?>

		 	</tr>				
			<tr>
				<form action="search.php" method="GET">
				<th colspan="4
				">
					<input type="text" name="keyword">
					<input type="submit" value="Find" name="submit">
				</th>	
				</form>
			</tr>		
			<tr>
				<th><a href="create.php">Add Employee</a></th>
				<th colspan="2"><a href="pdf.php">Dowanload as pdf</a></th>
				<th><a href="#">Recycle Bin</a></th>

			</tr>
			<tr>
				<th>Sl no:</th>
				<th>Name</th>
				<th>Dedignation</th>
				<th>Action</th>
			</tr>


			<?php 
				$Slno = 1+$offsetdata;
				foreach($empInfo as $info){ ?>
					<tr>
						<td><?php echo $Slno++; ?></td>
						<td><?php echo $info['emp_title']; ?></td>
						<td><?php echo $info['emp_designation']; ?></td>
						<td>
							<a href="show.php?id=<?php echo $info['emp_id']; ?>">Details</a> ||
							<a href="edit.php?id=<?php echo $info['emp_id']; ?>">Edit</a>||
							<a href="delete.php?id=<?php echo $info['emp_id']; ?>">Delete</a>
						</td>
					</tr>	
			<?php }	?>
			<tr>
				<td colspan="4">
					<?php 
					if ($totalRows > $numbOfItemPerPage) {
						for ($i = 1; $i <= $numberOfPage; $i++) { ?>
						<a href="index.php?pageNo=<?php echo $i ;?>"><?php echo $i." | ";?></a>
							
					<?php	} } ?>
				</td>
			</tr>
		</table>
		
	</section>
</body>
</html>
